/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.werapan.databaseproject.compronent;

import com.werapan.databaseproject.model.Product;

/**
 *
 * @author pattarapon
 */
public interface BuyProductable {
    public void buy(Product product,int qty);
}
